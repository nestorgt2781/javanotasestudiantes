/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notasstudents;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author nesto
 */
public class EstudianteTableModel extends AbstractTableModel{
    private List<String> columnNames;
    private List<Estudiante> data;
    
    
    public EstudianteTableModel(){
     super();   
     columnNames= new ArrayList<String>();
     data=new ArrayList<Estudiante>();
    }
    public EstudianteTableModel(List<String> ColumnNames, List<Estudiante> data) {
        this.columnNames = ColumnNames;
        this.data = data;
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.size();
    }

    @Override
    public Object getValueAt(int row, int col) {
        
        if (data.isEmpty()) {
            return null;
        }

        if (row < 0 || row >= data.size()) {
            return null;
        }

        List<String> docente = data.get(row).toList();

        if (col < 0 || col >= docente.size()) {
            return null;
        }

        return docente.get(col);
    }
    @Override
    public void setValueAt(Object value, int row, int col) {
        if (row < 0 || row >= data.size()) {
            return;
        }
        List<String> rowEstudiante = data.get(row).toList();
        if (col < 0 || col >= rowEstudiante.size()) {
            return;
        }

        rowEstudiante.set(col, value.toString());
        data.set(row, new Estudiante(rowEstudiante));
        fireTableCellUpdated(row, col);

    }
     @Override
    public String getColumnName(int column) {
        return columnNames.get(column);
    }

    public int addRow() {
        return addRow(new Estudiante());
    }

    public int addRow(Estudiante row) {
        data.add(row);
        fireTableRowsInserted(data.size() - 1, data.size() - 1);
        return data.size() - 1;
    }
    public void deleteRow(int row) {
        if (row < 0) {
            return;
        }

        data.remove(row);
        fireTableRowsDeleted(row, row);
    }
    public void SetData(ArrayList<Estudiante> est){
        data=est;
        String[] names = {"Id","Carnet", "Nombres", "Apellidos", "Bimestrel", "Bimestrell", "NotaFinal", "Estatus"};
        columnNames = Arrays.asList(names);        
    }
    
}
