/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notasstudents;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nesto
 */
public class ImplementData {
    
        private RandomAccessFile fichero = null;
        private RandomAccessFile header=null;
        private int Size=240;
        
        public  RandomAccessFile flujo;
        public  int numeroRegistros;
        public  int tamañoRegistro=240;
        
    public ImplementData() {
    }
    public  void Open(File archivo) throws IOException{
            try {
                flujo= new RandomAccessFile(archivo,"rw");
                numeroRegistros=(int)Math.ceil((double)flujo.length()/(double)tamañoRegistro);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ImplementData.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    public  void Close() throws IOException{
        flujo.close();
    }
    public  boolean Update(int i,Estudiante estudiante){
        if(i>=0 && i<=getNuemeroRegistros())
        {
            try {
                Open( new File("hestudiantes.data"));
                flujo.seek((i)*tamañoRegistro);
                flujo.writeInt(estudiante.Id);
                flujo.writeUTF(estudiante.Carnet);
                flujo.writeUTF(estudiante.Nombres);
                flujo.writeUTF(estudiante.Apellidos);
                flujo.writeUTF(estudiante.Bimestrel);
                flujo.writeUTF(estudiante.Bimestrell);
                flujo.writeUTF(estudiante.NotaFinal);
                flujo.writeUTF(estudiante.Estatus);
                Close();
            } catch (IOException ex) {
                Logger.getLogger(ImplementData.class.getName()).log(Level.SEVERE, null, ex);
            }
            return true;
        }else{
            return false;
        }
    }
    public  void Create(Estudiante estudiante){
        if(Update(getNuemeroRegistros(),estudiante)){
            
        }
        numeroRegistros++;
    }
    public  int getNuemeroRegistros(){
        int x=0;    
        try {
                Open(new File("hestudiantes.data"));
                 x=(int)Math.ceil((double)flujo.length()/(double)tamañoRegistro);
                Close();                
            } catch (IOException ex) {
                Logger.getLogger(ImplementData.class.getName()).log(Level.SEVERE, null, ex);
            }
            return x;
    }
    public  Estudiante getEstudiante(int i){
        Estudiante est=null;
        if(i>=0 && i<=getNuemeroRegistros()){
            try {
                flujo.seek(i*tamañoRegistro);
                est= new Estudiante(flujo.readInt(),flujo.readUTF(),flujo.readUTF(),flujo.readUTF(),flujo.readUTF(),flujo.readUTF(),flujo.readUTF(),flujo.readUTF());
            } catch (IOException ex) {
                Logger.getLogger(ImplementData.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            return null;
        }
        return est;
    }
    public  void Delete(int estudiante){
        ArrayList<Estudiante> dats=All();
        File g= new File("hestudiantes.data");
        g.delete();
        int cont=0;
        for (int i=0;i<dats.size()-1;i++) {
            if((estudiante)!=dats.get(i).Id){
                Create(dats.get(i));
            }
            cont++;
        }
        numeroRegistros--;
    }
    public  ArrayList<Estudiante> All(){
        ArrayList<Estudiante> data=new ArrayList<>();    
        try {
                Open(new File("hestudiantes.data"));
                for(int i=0;i<numeroRegistros;i++){
                    flujo.seek(i*tamañoRegistro);
                    data.add(new Estudiante(flujo.readInt(),flujo.readUTF(),flujo.readUTF(),flujo.readUTF(),flujo.readUTF(),flujo.readUTF(),flujo.readUTF(),flujo.readUTF()));
                }
                Close();
            } catch (IOException ex) {
                Logger.getLogger(ImplementData.class.getName()).log(Level.SEVERE, null, ex);
            }
        return data;
    }


}