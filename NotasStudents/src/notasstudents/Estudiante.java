/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notasstudents;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nesto
 */
public class Estudiante {
 
        public int Id ;
        public String Carnet ;
        public String Nombres;
        public String Apellidos ;
        public String Bimestrel ;
        public String Bimestrell ;
        public String NotaFinal ;
        public String Estatus ;

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getCarnet() {
        return Carnet;
    }

    public void setCarnet(String Carnet) {
        this.Carnet = Carnet;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public String getBimestrel() {
        return Bimestrel;
    }

    public void setBimestrel(String Bimestrel) {
        this.Bimestrel = Bimestrel;
    }

    public String getBimestrell() {
        return Bimestrell;
    }

    public void setBimestrell(String Bimestrell) {
        this.Bimestrell = Bimestrell;
    }

    public String getNotaFinal() {
        return NotaFinal;
    }

    public void setNotaFinal(String NotaFinal) {
        this.NotaFinal = NotaFinal;
    }

    public String getEstatus() {
        return Estatus;
    }

    public void setEstatus(String Estatus) {
        this.Estatus = Estatus;
    }
            
        public Estudiante() { }

        public Estudiante(int id, String carnet, String nombres, String apellidos, String bimestrel, String bimestrell, String notaFinal, String estatus)
        {
            Id = id;
            Carnet = carnet;
            Nombres = nombres;
            Apellidos = apellidos;
            Bimestrel = bimestrel;
            Bimestrell = bimestrell;
            NotaFinal = notaFinal;
            Estatus = estatus;
        }
        public Estudiante(List<String> estudianteList) {
            Id = Integer.parseInt(estudianteList.get(0));
            Carnet = estudianteList.get(1);
            Nombres = estudianteList.get(2);
            Apellidos = estudianteList.get(3);
            Bimestrel = estudianteList.get(4);
            Bimestrell = estudianteList.get(5);
            NotaFinal = estudianteList.get(6);
            Estatus = estudianteList.get(7);  
    }
        public List<String> toList() {
        List<String> student = new ArrayList<>();

        student.add(String.valueOf(Id));
        student.add(Carnet);
        student.add(Nombres);
        student.add(Apellidos);
        student.add(Bimestrel);
        student.add(Bimestrell);
        student.add(NotaFinal);
        student.add(Estatus);
        return student;

    }
}
